fn main(){
    let s = String::from("Hello");
    let r = dangle(&s);
    println!("{}", *r);
}

// this fn is invalid
fn dangle(s:&String)->&String {
    // let s = String::from("HELLO");
    return &s;
}