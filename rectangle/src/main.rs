#[derive(Debug)]
struct Rectangle{
    width: u32, 
    height: u32,
}

impl Rectangle{
    fn area(&self)-> u32{
        self.width * self.height
    }

    fn square(size:u32)->Rectangle{
        Rectangle{
            width:size,
            height: size,
        }
    }
}

fn main() {
    let rect = Rectangle{
        width:30,
        height: 50,
    };
    println!("the area value is:{}", rect.area());
    println!("{:#?}", rect);
}
