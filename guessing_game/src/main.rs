use std::io;
use rand::Rng; // trait
use std::cmp::Ordering;
fn main() {
    println!("Guess Num!");

    let secret_num = rand::thread_rng().gen_range(1, 101);
    println!("get secret num:{}", secret_num);
    loop {
        println!("Please input a number");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("cannot read!");
        let guess:u32 = match guess.trim().parse(){
            Ok(num) => num,
            Err(_) => {println!("Please enter a num, your input:{}", guess);continue},
        };
        println!("your input number is: {}", guess);
        match guess.cmp(&secret_num){
            Ordering::Less => println!("Too small"),
            Ordering::Equal => {println!("Equal! You win!"); break},
            _ => println!("Other!"),
        }
    }


}
